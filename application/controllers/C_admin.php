<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
Puskesmas
PuskesmasAmbulu 
Poli Gigi

Tampilan

proses:
- Jumlah Obat 10 teratas


UI
navbar -warna

===///===

done:

0
superadmin (id=0)
- allfeatur

1
poligigi
cuma 1
- laporan keseluruhan

2
gudang
- semua, minus stok
- graf, perlaporan

3
kepala
- pemakaian
- permintaan
- graf, perlaporan

Mia:
?
nama diganti?
Nama - puskesmas

d
1 login
bg gambar
dDashboard - home
dTipe obat
Botol, ampul, biji, set, rol, tube

2 User
nambah admin
crud
*/

class C_admin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_admin');
		$this->load->helper(array('form', 'url'));
		if(!$this->session->userdata('logged_in_admin')){
			redirect('login');
		}
		$this->load->model('M_obat');
		$this->load->model('M_penerimaan');
		$this->load->model('M_permintaan');
		$this->load->model('M_pemakaian');
		$this->load->model('M_stok');
		// $this->load->model('M_user');
	}
	
	public function index($page="", $menuName="dashboard"){
		if($_SESSION['logged_in_admin']){
			$obat = $this->M_stok->readStok()->result_array();
			$stokObat = $this->laporan->readStok()->result_array();
			$today = date('Y-m-d');
			$exp = date('Y-m-d', strtotime("+60 days"));	
			
			$expDate = $this->M_stok->readExp($today, $exp)->num_rows();

			$data = array('var' => $page,'menuName'=>$menuName, 'jumlahObat'=>$obat, 'exp'=>$expDate, 'result'=>$stokObat);
			$data['tbobat'] = $this->M_stok->readStokMost()->result_array();

			$this->load->view("admin/layout/header", $data);
			$this->load->view("admin/index", $data);
			$this->load->view("admin/layout/footer");
		}else{
			redirect('login');
		}
	}

	public function login(){
			
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		if ($this->form_validation->run() == FALSE){
			$this->load->view('admin/login');
		}else{
			$this->M_admin->login_authen();
			if ($_SESSION['logged_in_admin'] == TRUE) {
				redirect(base_url());
			}else{
				echo ('<script type="text/javascript">window.alert("User12 tidak terdaftar");window.location.href="'.base_url("login").'" </script>');
			}
		}
	}

	public function obat($menuName="obat"){
		if($_SESSION['logged_in_admin']){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('nama_obat', 'nama_obat', 'required');
			$this->form_validation->set_rules('no_obat', 'no_obat', 'required');
			$this->form_validation->set_rules('tipe', 'tipe', 'required');
			
			if ($this->form_validation->run() == FALSE){
				$nextId = $this->M_obat->getLastId()+1;
				
				$data = array('menuName'=>$menuName,
					'nextId' => $nextId
				);

				$data['otipe']=$this->M_obat->get_allobattipe()->result();
				$this->load->view("admin/layout/header",$data);
				$this->load->view("admin/obat");
				$this->load->view("admin/layout/footer");
			}
			else{
				$this->M_obat->createObat();
				redirect('obat');
			}
		}
		else
		redirect('login');
	}

	//distribusi
	public function distribusi($menuName="distribusi"){
		if($_SESSION['logged_in_admin']){
			$this->load->library('form_validation');
			$obat = $this->M_obat->read()->result_array();

			$this->form_validation->set_rules('batch_no', 'batch_no', 'required');
			$this->form_validation->set_rules('jmlinput', 'jmlinput', 'required');
			
			if ($this->form_validation->run() == FALSE){
				$data = array('menuName'=>$menuName, 'obat'=>$obat,
				'nextId' => $this->M_pemakaian->getLastId()+1
				);
				$data['stokxbatch']=$this->M_stok->allStok(true)->result();

				$this->load->view("admin/layout/header",$data);
				$this->load->view("admin/distribusi",$data);
				$this->load->view("admin/layout/footer");
			}else{
				// $this->M_pemakaian->create();
			}
		}
		else
		redirect('login');
	}

	public function dbdistribusi($batch_no=0){
		//cek jml obat
		//get jml obat pd batch
		// $where['batch_no']=$batch_no;
		$where['batch_no']=$this->input->post('batch_no');
		$row=$this->M_stok->getStokObatBybatch($where)->result();
		// var_dump($row);
		$id=$row[0]->id;
		$jmlobat=$row[0]->jumlah_stok;

		$jmldist=$this->input->post('jmldist');
		if($jmlobat<$jmldist){
			echo "kurang : jmlobat < jmldist";
		}else{
			$adata['batch']=$row[0]->batch_no;
			$adata['noobat']=$row[0]->no_obat;
			$adata['jml']=$this->input->post('jmlinput');
			$adata['tipetrans']=1;
			// $adata['tipetrans']=1;
			$udata=[];
			// $udata['stokpoligigi']="stokpoligigi+".$adata['jml'];
			// $udata['stokgudang']="stokgudang-".$adata['jml'];

			$ret=$this->M_obat->distribusi($id,$adata,$udata);
			$this->session->set_flashdata('msg',$row[0]->batch_no." : ".$row[0]->nama_obat);
			
			redirect('distribusi');
			// var_dump($ret);
			// echo "sukses";
		}
	}


	public function pemakaian($menuName="pemakaian"){
		if($_SESSION['logged_in_admin']){
			$this->load->library('form_validation');
			$obat = $this->M_obat->read()->result_array();

			$this->form_validation->set_rules('no_pemakaian', 'no_pemakaian', 'required');
			$this->form_validation->set_rules('tgl_pemakaian', 'tgl_pemakaian', 'required');
			$this->form_validation->set_rules('no_obat', 'no_obat', 'required');
			$this->form_validation->set_rules('jumlah_pemakaian', 'jumlah_pemakaian', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$data = array('menuName'=>$menuName, 'obat'=>$obat,
								'nextId' => $this->M_pemakaian->getLastId()+1					
			);
				$this->load->view("admin/layout/header",$data);
				$this->load->view("admin/pemakaian");
				$this->load->view("admin/layout/footer");
			}else{
				$this->M_pemakaian->create();
			}
		}
		else
		redirect('login');
	}

	public function print($jenisLaporan='stok',$th=0,$bln=0){
		
		if($_SESSION['logged_in_admin']){
			$result=array();
			$gr=0;
			switch ($jenisLaporan) {
				case 'stok':
					$result = $this->laporan->readStok()->result_array();
					break;
				case 'pemakaian':
					$result = $this->laporan->readPemakaian($th,$bln)->result_array();
					$gr=$this->laporan->pemakaian_bybulan(3)->result();
					break;
				case 'penerimaan':
					$result = $this->laporan->readPenerimaan($th,$bln)->result_array();
					break;
				case 'permintaan':
					$result = $this->laporan->readPermintaan($th,$bln)->result_array();
					break;
				case 'keseluruhan':
					$result = $this->laporan->readLaporan($th,$bln)->result_array();
					break;
				default:
					$result = $this->laporan->readStok()->result_array();
					break;
			}
			
			$data = array('result'=>$result,'jenisLaporan'=>$jenisLaporan);
			$data['gr']=$gr;
			$this->load->view("admin/print", $data);
		}
		else
		redirect('login');
	}
	public function permintaan($menuName="permintaan"){
		if($_SESSION['logged_in_admin']){
			$this->load->library('form_validation');
			$obat = $this->M_obat->read()->result_array();

			$this->form_validation->set_rules('no_permintaan', 'no_permintaan', 'required');
			$this->form_validation->set_rules('tgl_permintaan', 'tgl_permintaan', 'required');
			$this->form_validation->set_rules('nama_obat', 'nama_obat', 'required');
			$this->form_validation->set_rules('jumlah', 'jumlah', 'required');
			
			
			if ($this->form_validation->run() == FALSE)
			{
				$data = array('menuName'=>$menuName, 'obat'=>$obat,
								'nextId' =>$this->M_permintaan->getLastId()+1
			);
				$this->load->view("admin/layout/header",$data);
				$this->load->view("admin/permintaan");
				$this->load->view("admin/layout/footer");
			}
			else{
				$this->M_permintaan->create();
				redirect('permintaan');
			}
		}
		else
		redirect('login');
	}
	public function penerimaan($menuName="penerimaan"){
		if($_SESSION['logged_in_admin']){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('batch_no', 'batch_no', 'required');
			$this->form_validation->set_rules('tgl_penerimaan', 'tgl_penerimaan', 'required');
			if($menuName=="penerimaan"){
				$this->form_validation->set_rules('nama_obat', 'nama_obat', 'required');
				$this->form_validation->set_rules('jumlah', 'jumlah', 'required');
			}else{
				$this->form_validation->set_rules('noobat', 'noobat', 'required');
				$this->form_validation->set_rules('jmlinput', 'jumlah', 'required');
			}

			$this->form_validation->set_rules('exp_date', 'exp_date', 'required');
			$obat = $this->M_obat->read()->result_array();
			if ($this->form_validation->run() == FALSE){
				$data=[];
				if($menuName==="penerimaan"){
					$data = array('menuName'=>$menuName, 'obat'=>$obat,
						'nextId'=>$this->M_penerimaan->getLastId()+1,
						'listObat'=> json_encode($this->M_obat->getObat()),
						'page'=>"Penerimaan"
					);
				// }else if($menuName==="gudang"){
				}else{
					$data = array('menuName'=>"gudang",
						'obat'=>$obat,
						//awas, get dari m_obat
						'nextId'=>$this->M_obat->get_maxbatch()->result()[0]->maxbatch+1,
						'listObat'=> json_encode($this->M_obat->getObat()),
						'page'=>"Penerimaan Gudang"
					);
				}
				// var_dump($this->input->post());
				// exit();
				$this->load->view("admin/layout/header",$data);
				$this->load->view("admin/penerimaangudang", $data);
				$this->load->view("admin/layout/footer");
			// $this->load->view("admin/script/penerimaan-script",$data);
			}else{
				//dibb
				// echo "dd <br>";
				// var_dump($this->input->post());
				// exit();

				if($menuName==="penerimaan"){
					$this->M_penerimaan->create();
					redirect('penerimaan');
				}else{
					$batch_no = $this->input->post('batch_no');
					$noobat = $this->input->post('noobat');
					$jmlinput = $this->input->post('jmlinput');
					$exp_date = $this->input->post('exp_date');
					$tgl_penerimaan = $this->input->post('tgl_penerimaan');
					
					//ke tabel obat trans
					$adata[0]['batch']=$batch_no;
					$adata[0]['noobat']=$noobat;
					$adata[0]['jml']=$jmlinput;
					$adata[0]['tipetrans']=2;
					$adata[0]['waktutrans']=$tgl_penerimaan;
					
					//ke tabel obat stok obat
					$adata[1]['batch_no']=$batch_no;
					$adata[1]['no_obat']=$noobat;
					$adata[1]['stokgudang']=$jmlinput;
					$adata[1]['jumlah_stok']=$jmlinput;
					$adata[1]['exp_date']=$exp_date;

					//0 = arah ke gudang
					$ret=$this->M_obat->distribusi(null,$adata,null,0);
					// $this->M_obat->distribusi();
					redirect('penerimaan/gudang');
				}
			}
		}else{

			redirect('login');
		}
	}
	
	public function logout(){
		session_destroy();
		redirect('');
	}

	// public function laporanPemakaian($menuName="laporanPemakaian", $subMenu=""){
	public function laporanPemakaian($th=0,$bln=0){
		if($_SESSION['logged_in_admin']){
			$menuName="laporanPemakaian";
			//table
			$result = $this->laporan->readPemakaian($th,$bln)->result_array();
			
			$data = array('menuName'=>$menuName,'result'=>$result);
			// $gr=$this->laporan->pemakaian_bybulan(3)->result();
			// $grtahun=$this->laporan->pemakaian_pertahun(date('Y'))->result();

			//select 5 besar obat (pemakaian) dlm setahun
			$pemakaian_pth=$this->laporan->pemakaian_pertahun(date('Y'),true)->result();
			$data['topobat']=$pemakaian_pth;
			//get id lalu di loop
			foreach($pemakaian_pth as $r){
				//get jumlah byobat
				$pemakaian=$this->laporan->pemakaian_byobat($r->no_obat,date('Y'))->result();
				$data['gpemakaian'.$r->no_obat]=$pemakaian;
			}
			$data['th']=$th;
			$data['bln']=$bln;

			// var_dump($data);
			// exit();
			// $data['year']=date('Y');

			$this->load->view("admin/layout/header",$data);
			$this->load->view("admin/laporanPemakaian",$data);
			$this->load->view("admin/layout/footer",$data);
		}
		else
		redirect('login');
	}

	// public function laporanPenerimaan($menuName="laporanPenerimaan"){
	public function laporanPenerimaan($th=0,$bln=0){
		if($_SESSION['logged_in_admin']){
			$menuName="laporanPenerimaan";

			$result = $this->laporan->readPenerimaan($th,$bln)->result_array();
			$data = array('menuName'=>$menuName,'result'=>$result);
			
			//select 5 besar obat (pemakaian) dlm setahun
			$penerimaan_pth=$this->laporan->penerimaan_pertahun(date('Y'),true)->result();
			$data['topobat']=$penerimaan_pth;
			//get id lalu di loop
			foreach($penerimaan_pth as $r){
				//get jumlah byobat
				$penerimaan=$this->laporan->penerimaan_byobat($r->no_obat,date('Y'))->result();
				$data['gpenerimaan'.$r->no_obat]=$penerimaan;
			}
			$data['th']=$th;
			$data['bln']=$bln;

			$this->load->view("admin/layout/header",$data);
			$this->load->view("admin/laporanPenerimaan",$data);
			$this->load->view("admin/layout/footer",$data);
		}
		else
		redirect('login');
	}

	// public function laporanPermintaan($menuName="laporanPermintaan", $subMenu=""){
	public function laporanPermintaan($th=0,$bln=0){
		if($_SESSION['logged_in_admin']){
			$menuName="laporanPermintaan";
			$result = $this->laporan->readPermintaan($th,$bln)->result_array();
			$data = array('menuName'=>$menuName,'result'=>$result);
			
			//select 5 besar obat (pemakaian) dlm setahun
			$permintaan_pth=$this->laporan->permintaan_pertahun(date('Y'),true)->result();
			$data['topobat']=$permintaan_pth;
			//get id lalu di loop
			foreach($permintaan_pth as $r){
				//get jumlah byobat
				$permintaan=$this->laporan->permintaan_byobat($r->no_obat,date('Y'))->result();
				$data['gpermintaan'.$r->no_obat]=$permintaan;
			}
			$data['th']=$th;
			$data['bln']=$bln;

			$this->load->view("admin/layout/header",$data);
			$this->load->view("admin/laporanPermintaan",$data);
			$this->load->view("admin/layout/footer",$data);
		}
		else
		redirect('login');
	}

	// public function laporanKeseluruhan($menuName="laporanKeseluruhan", $subMenu=""){
	public function laporanKeseluruhan($th=0,$bln=0){
		if($_SESSION['logged_in_admin']){
			$menuName="laporanKeseluruhan";
			$lv=$this->session->userdata('usrlv');
			
			$gudang=$lv==2?true:false;
			// $result = $this->laporan->readLaporan($gudang,$th,$bln)->result_array();
			$result = $this->laporan->readLaporan($gudang,$th,$bln)->result_array();
			$data = array('menuName'=>$menuName,'result'=>$result);
			$data['gudang']=$gudang;
			$data['lv']=$lv;
			
			$data['th']=$th;
			$data['bln']=$bln;

			$this->load->view("admin/layout/header",$data);
			$this->load->view("admin/laporan1",$data);
			$this->load->view("admin/layout/footer",$data);
		}
		else
		redirect('login');
	}

	public function laporanKeseluruhanGudang($a=0,$th=0,$bln=0){
		if($_SESSION['logged_in_admin']){
			$stokobat=$this->laporan->getstok()->result();
			$arr=[];
			$n=1;
			foreach($stokobat as $so){
				$trans=$this->laporan->gettrans($so->batch_no)->result();
				//transaksi size
				$trsize=sizeof($trans);
				// $trsize=$trsize==0?1:sizeof($trans);
				// $trsize=$trsize==0?1:sizeof($trans);
				$arr[]=[
					"nx"=>$n,
					"obat"=>$so,
					"trsize"=>$trsize,
					"obattrans"=>$trans
				];
				$n++;
			}
			$data=[
				"menuName"=>"gudang",
				"obat"=>$arr,
				"th"=>$th,
				"bln"=>$bln,
			];
			if($a==0){
				$this->load->view("admin/layout/header",$data);
				$this->load->view("admin/laporan",$data);
				$this->load->view("admin/layout/footer",$data);
			}else{
				var_dump($arr);
			}
		}else{
			redirect('login');
		}
	}

	public function laporanStok($menuName="laporanStok", $subMenu=""){
		if($_SESSION['logged_in_admin']){
			$lv=$this->session->userdata('usrlv');
			//jika gudang
			
			$gudang=$lv==2?true:false;
			$result = $this->laporan->readStok($gudang)->result_array();
			$data = array('menuName'=>$menuName,'result'=>$result);
			$data['gudang']=$gudang;
			$data['lv']=$lv;
			$this->load->view("admin/layout/header",$data);
			if(!$gudang){
				$this->load->view("admin/laporanStok",$data);
			}else{
				$this->load->view("admin/laporanStok",$data);
				// $this->load->view("admin/laporanStokGudang",$data);
			}
			$this->load->view("admin/layout/footer");
		}
		else
		redirect('login');
	}

	public function laporanStokDetail($subMenu=""){
		if($_SESSION['logged_in_admin']){
			$menuName="laporanStokDetail";
			$lv=$this->session->userdata('usrlv');
			//jika gudang
			
			$gudang=$lv==2?true:false;
			$result = $this->laporan->readStokDetail($gudang)->result_array();
			$data = array('menuName'=>$menuName,'result'=>$result);
			$data['gudang']=$gudang;
			$data['lv']=$lv;
			$this->load->view("admin/layout/header",$data);
			if(!$gudang){
				$this->load->view("admin/laporanStokDetail",$data);
			}else{
				$this->load->view("admin/laporanStokDetail",$data);
				// $this->load->view("admin/laporanStokGudang",$data);
			}
			$this->load->view("admin/layout/footer");
		}
		else
		redirect('login');
	}

	//by dib
	// users

	function alluser(){
		$this->load->model('m_user');
		$data['menuName']="alluser";
		$data['users']=$this->m_user->getalluser()->result();
		$data['usrlv']=$this->m_user->getalluserlevel()->result();
		$this->load->view('admin/userall',$data);
	}

	function ajax_singleuser($id=0){
		$this->load->model('m_user');
		$id=$this->input->post('id');
		$row=$this->m_user->getuser($id)->result();
		$data['status']=1;
		$data['data']=$row;
		echo json_encode($data);
	}

	function ajax_userdbadd(){
		$this->load->model('m_user');
		// $wh['id']=$this->input->post('idusr');
		$udata['username']=$this->input->post('username1');
		$udata['password']=$this->input->post('password1');
		$udata['idlevel']=$this->input->post('idlevel');
		$q=$this->m_user->create($udata);
		// var_dump($udata);
		// var_dump($this->input->post());
		// var_dump($q);
		$data['status']=1;
		$data['data']="sukses";
		echo json_encode($data);
	}

	function ajax_userdbedit(){
		$this->load->model('m_user');
		$wh['id']=$this->input->post('idusr');
		$udata['username']=$this->input->post('username1');
		$udata['password']=$this->input->post('password1');
		$udata['idlevel']=$this->input->post('idlevel');
		$q=$this->m_user->update($wh['id'],$udata);
		// var_dump($udata);
		// var_dump($this->input->post());
		// var_dump($q);
		$data['status']=1;
		$data['data']="sukses";
		echo json_encode($data);
	}

	function user_edit($id){
		$this->load->model('m_user');
		$data['user']=$this->m_user->getalluser();
		$this->load->view('admin/useredit');
	}

	function dbuser_insert(){
		$this->load->model('M_user');
		// $data['']
		// $this->input->post('');
		$data['username']=$this->input->post('username');
		$data['password']=$this->input->post('password');
		$data['idlevel']=$this->input->post('idlevel');
		$q=$this->m_user->create($data);
		// $this->load->view('admin/userall');
	}

	function dbuser_update(){
		// $this->load->model('M_user');
	}
	
	function dbuser_delete($id=0){
		$this->load->model('M_user');
		// $id=$this->input->post('iduser');
		if(!is_numeric($id)||$id==1||$id==0){
			redirect('alluser');
			return;
		}else{
			$q=$this->M_user->delete($id);
		}
		redirect('alluser');
	}

	// function alluser(){}
	// function alluser(){}
	// function alluser(){}
	// function alluser(){}
}
