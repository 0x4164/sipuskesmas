<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
Puskesmas
PuskesmasAmbulu 
Poli Gigi
*/

class Auth extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_admin');
		if($this->session->userdata('logged_in_admin')){
			redirect('obat');
		}
		$this->load->model('M_obat');
		$this->load->model('M_penerimaan');
		$this->load->model('M_permintaan');
		$this->load->model('M_pemakaian');
		$this->load->model('M_stok');
		// $this->load->model('M_user');
		$this->load->helper(array('form', 'url'));

	}
	
	public function index(){
		$this->login();
	}

	public function login(){
			
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		if ($this->form_validation->run() == FALSE){
			$this->load->view('admin/login');
		}else{
			$this->M_admin->login_authen();
			if ($_SESSION['logged_in_admin'] == TRUE) {
				redirect(base_url());
			}else{
				echo ('<script type="text/javascript">window.alert("User12 tidak terdaftar");window.location.href="'.base_url("login").'" </script>');
			}
		}
	}
}
