  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Input | Distribusi
        <small>Poli Gigi Puskesmas Ambulu</small>
      </h1>
    </section>
    <section class="content">
      <?php 
      $msg="";
      if($this->session->flashdata('msg')){
        $msg=$this->session->flashdata('msg');
      }
      // var_dump($this->session->flashdata('msg'));
      if($msg){
      ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Sukses!</h4>
        Berhasil terdistribusi ke poligigi : <?php echo $msg;?>
      </div>
      <?php } ?>
      <div class="row">
        <div class="col-xs-8 col-md-offset-2">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Input Distribusi</h3>
            </div>
						<?php $attributes = array('class' => 'form-horizontal','id' => 'formPenerimaan');
            echo form_open('dbdistribusi', $attributes); ?>
              <div class="box-body">
                <!-- <div class="form-group">
                  <label class="col-sm-2 control-label">Nomor Pemakaian</label>
                  <div class="col-sm-10">
                    <input name="no_pemakaian" type="text" class="form-control" placeholder="Nomor" value="<?php echo $nextId ?>" readonly>
                  </div>
                </div> -->
                <div class="form-group">
                  <label class="col-sm-2 control-label">Batch</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="batch_no" id="batch_no">
                    <?php foreach($stokxbatch as $r){ ?>
                      <option value="<?php echo $r->id;?>"><?php echo $r->nama_obat."-".$r->id." tersisa : ".$r->stokgudang;?></option>
                    <?php }?>
                    </select>
                    <!-- <input name="no_pemakaian" type="text" class="form-control" placeholder="Nomor" value="<?php echo $nextId ?>" readonly> -->
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-10">
                    <input name="x" type="text" class="form-control" placeholder="Tgl, bln, thn Pemakaian" value="<?php echo date('Y-m-d') ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Jumlah Distribusi</label>
                  <div class="col-sm-10">
                    <input name="jmlinput" type="text" class="form-control" placeholder="Jumlah Distribusi">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
            <?php echo form_close() ?>
          </div>
        </div>
      </div>
    </section>
  </div>

  