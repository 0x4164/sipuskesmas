<?php
$this->load->view('admin/layout/header');
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Semua user
        <!-- <small>Poli Gigi Puskesmas Ambulu</small> -->
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">User</h3>
              <button id="btformtambah" type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
              Tambah</button>
            </div>
            <div class="box-body">
              <!-- <table id=""> -->
              <table id="tbuser" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Level</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                <?php ?>
                <?php 
                $n=1;
                foreach($users as $u){
                  ?>
                  <tr>
                    <td><?php echo $n;?></td>
                    <td><?php echo $u->username;?></td>
                    <td><?php echo $u->password;?></td>
                    <td><?php echo $u->idlevel;?></td>
                    <td>
                    <button type="button" class="btn btn-default" 
                    onclick="lihat_detil(<?php echo $u->id;?>)"
                    data-toggle="modal" data-target="#modal-default">
                    O
                    </button>
                    <a class="btn btn-danger" href="<?php echo base_url('c_admin/dbuser_delete/'.$u->id);?>" title="Delete user" onclick="return confirm('Yakin?')">X</a>
                    </td>
                  </tr>
                  <?php
                  $n++;
                }
                ?>
                </tbody>
                <tfoot>
                  <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Password</th>
                      <th>Level</th>
                      <th>Aksi</th>
                    </tr>
                </tfoot>
              </table>
              <!-- .tab-->
            </div>
          </div>
        </div>
      </div>
    </section>
  </div> 
  <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 id="title1" class="modal-title">Default Modal</h4>
              </div>
              <form id="formuser" method="post" value="0">
              <div class="modal-body">
                <!-- alert -->
                <div id="alert1">
                </div>
                <!-- <div class="alert alert-info alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-info"></i> Alert!</h4>
                  Info alert preview. This alert is dismissable.
                </div> -->
                <!-- ./alert -->
                <!-- <p>One fine body…</p> -->
                <input id="idusr" type="hidden" name="idusr" value="">
                  <div class="form-group">
                    <label>Username</label>
                    <input id="username" type="text" name="username1" class="form-control" placeholder="username" required>
                  </div>
                  <div class="form-group">
                    <label>Password</label>
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button id="btpass" type="button" class="btn">Tampil</button>
                      </div>
                      <input id="password" type="password" name="password1" class="form-control" placeholder="password" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <select class="form-control" id="idlevel" name="idlevel" required>
                      <option value="">Pilih level</option>
                    <?php // format ?>
                    <?php 
                  // $n=1;
                  foreach($usrlv as $ul){
                    ?>
                    <option value="<?php echo $ul->id;?>"><?php echo $ul->id."- ".$ul->level;?></option>
                  <?php }
                    ?>
                    </select>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
                </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
  <?php
$this->load->view('admin/layout/footer-usr');
?>