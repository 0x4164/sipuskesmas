<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Puskesmas Ambulu | Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url("assets/"); ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/"); ?>bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/"); ?>bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/"); ?>dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/"); ?>plugins/iCheck/square/blue.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
  /* https://www.w3schools.com/howto/howto_css_blurred_background.asp */
  .bg1{
    background-image: url(<?php echo base_url('assets/img/pusk1.jpg');?>);
    /* width:100%;
    height:100%; */
    /* Add the blur effect */
    /* filter: blur(8px);
    -webkit-filter: blur(8px); */

    /* Full height */
    height: 100%;

    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .grad1 {
    height: 200px;
    background-color: #cccccc00;
    /* background-image: radial-gradient(red, yellow); */
  }

  /* abuabu */
  .bgc1{
    background-color:#d2d6dedd;
  }
  </style>
</head>
<body class="hold-transition login-page bg1">

<div class="login-box bgc1">
  <div class="login-logo">
  <b>Puskesmas</b>Ambulu
  <br>
  <!-- Poli Gigi -->
  <img src="<?php echo base_url('assets/img/');?>logo_poligigi.jpg" alt="">
  </div>
  <div class="login-box-body">
    <p class="login-box-msg">Mohon Login untuk memulai</p>
    <?php echo form_open('login'); ?>
      <div class="form-group has-feedback">
        <input autofocus="" name="username" type="text" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4 pull-right">
					
					<?php $button = array('name' => 'login', 'class' => 'btn btn-primary btn-block btn-flat', 'type' => 'submit'); ?>
					<?php echo form_button($button,'Masuk'); ?>
        </div>
      </div>
    <?php echo form_close(); ?>

  </div>
</div>

<script src="<?php echo base_url("assets"); ?>bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url("assets/"); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url("assets/"); ?>plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' 
    });
  });
</script>
</body>
</html>
