<footer class="main-footer">
    <strong>Copyright &copy; 2018 <a href="#">Puskesmas Ambulu</a>.</strong> All rights
    reserved.
  </footer>
</div>

<script src="<?php echo base_url("assets/") ?>bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?php echo base_url("assets/") ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/morris.js/morris.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url("assets/") ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url("assets/") ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url("assets/") ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?php echo base_url("assets/") ?>dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url("assets/") ?>dist/js/pages/dashboard.js"></script>
<script src="<?php echo base_url("assets/") ?>dist/js/demo.js"></script>
<script src = "<?php echo base_url("assets/src/") ?>jquery.inputpicker.js"></script>
<?php 
$this->load->view('vfunctions');
?>
<script>
  $(function () {
    $('#tbuser').DataTable()
    // $('#example2').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // })
  })

  function malert(msg="",type=0){
    atype="";
    switch(type){
      case 0:atype="alert-success";break;
      case 1:atype="alert-danger";break;
      default :atype="alert-success";break;
    }
    format='<div class="alert '+atype+' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+msg+'</div>'
    $('#alert1').html(format);
  }
  function resetform(){
    $('#title1').empty().append('Tambah user');
    $('#idusr').val('');
    $('#username').val('');
    $('#password').val('');
    $('#idlevel').val('');
  }
  
  // show modal
  //by dib, the true
  function lihat_detil(id){
    $('#formuser').attr('value','0');
    $.post('<?php echo site_url('user/ajaxgetuser'); ?>', {
      id : id
    },function(data, status){
      // alert(id);
      var a = JSON.parse(data);
      console.log(a);
      var b = a.data[0];

      head=b.username;
      $('#title1').empty();
      $('#title1').append(head);
      $('#idusr').val(b.id);
      $('#username').val(b.username);
      $('#password').val(b.password);
      $('#idlevel').val(b.idlevel);
    });
  }

  $("#btformtambah").on('click',function(){
    // alert(1)
    // $('#head').append(head);
    resetform();
    // $('#formuser').attr('id','formuseradd');
    $('#formuser').attr('value','1');
  })

  //switch show pass
  $("#btpass").on('click',function(){
    intype=$("#password").attr('type')
    if(intype=="password"){
      $("#password").attr('type','text')
    }else{
      $("#password").attr('type','password')
    }
  })

  $('#formuser').submit(function(e){
    e.preventDefault();
    urlup="";
    type=$(this).attr('value');
    // alert($(this).attr('value'))
    if(type==0){
      urlup=urls.user.update;
    }else{
      urlup=urls.user.add;
    }

    // alert(urlup);
    $.ajax({
      url:urlup,
      type:"post",
      data:new FormData(this),
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data){
        // alert("Success.");
        console.log(data.status);
        // table.ajax.reload();
        if(type==0){
          // alert(1);
          malert("Update sukses, tekan F5",0)
        }else{
          malert("Add sukses, tekan F5",0)
        }
        // lihat_detil_post($('#userid').val());
        //reload table
      }
    });
  });
  
</script>
</body>
</html>
