<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="#">Puskesmas Ambulu</a>.</strong> All rights
    reserved.
  </footer>
</div>

<script src="<?php echo base_url("assets/") ?>bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?php echo base_url("assets/") ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/morris.js/morris.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url("assets/") ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url("assets/") ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?php echo base_url("assets/") ?>bower_components/chart.js/Chart.js"></script>

<script src="<?php echo base_url("assets/") ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url("assets/") ?>bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?php echo base_url("assets/") ?>dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url("assets/") ?>dist/js/pages/dashboard.js"></script>
<script src="<?php echo base_url("assets/") ?>dist/js/demo.js"></script>
<script src = "<?php echo base_url("assets/src/") ?>jquery.inputpicker.js"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })


</script>
<script>
  var classes = ["bg-blue", "bg-light-blue", "bg-teal","bg-cyan",];

  $(".info-box-2").each(function(){
      $(this).addClass(classes[~~(Math.random()*classes.length)]);
  });

  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    
    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    // var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
    // This will get the first returned node in the jQuery collection.
    // var areaChart       = new Chart(areaChartCanvas)
    mlabel1=['January', 'February', 'March', 'April', 'May', 'June', 'July']
    mlabel=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    mfillcolors=[
      '#00a65a',
      '#00a67a',
      '#00a69a',
      '#00a6ba',
      '#00a6da',
      '#00a6fa',
      '#00a65a',
      '#00a65a',
      '#00a65a',
      'rgba(60,141,188,0.9)']
    

    //get month

    datachart=[<?php

    //datapemakaian per tahun
    $colors=array();
    $colors[]='#00c0ef';
    $colors[]='#00a65a';
    $colors[]='#f39c12';
    $colors[]='#f56954';
    $colors[]='#d2d6de';
    $colors[]='#ff851b';

      // var_dump($colors);
      // $json=array();
      // '#00a65a' ijo
      $m1=$menuName=="laporanPemakaian";
      $m2=$menuName=="laporanPenerimaan";
      $m3=$menuName=="laporanPermintaan";
      
      $n=0;
      if(isset($topobat)){ 
        foreach($topobat as $r1){
          //out json array
          ?>
          {
            label               : "<?php echo $r1->no_obat?>",
            fillColor           : '<?php echo $colors[$n];?>',
            strokeColor         : '<?php echo $colors[$n];?>',
            pointColor          : '<?php echo $colors[$n];?>',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: mfillcolors[1],
            data                : [<?php
            
            $gdata=array();
            //careful eval usage may useful
            if($m1){
              eval('$gdata = $gpemakaian'.$r1->no_obat.";");
            }else if($m2){
              eval('$gdata = $gpenerimaan'.$r1->no_obat.";");
            }else if($m3){
              eval('$gdata = $gpermintaan'.$r1->no_obat.";");
            }
            
            // echo "abcde";

            // var_dump($gdata);

            $b=1;

            //hmmm
            $dataval=[0,0,0,0,0,0,0,0,0,0,0,0];
            foreach($gdata as $r2){
              $blndata=$r2->blndata;
              $dataval[$blndata-1]=$r2->tot;
            }
            foreach($dataval as $d){
              echo $d.",";
            }
            // var_dump($dataval);
            ?>]
          },
          <?php
          // $json[]=$d;
          $n++;
        }
      }
      // echo json_encode($json);
    ?>]

    var areaChartData = {
      labels  : mlabel,
      datasets: datachart
    }

    //-------------
    //- BAR CHART -
    //-------------
    // var barChartCanvas                   = $('#barChart2').get(0).getContext('2d')
    var barChartCanvas                   = $('#barChart2').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    barChartData.datasets[1].fillColor   = '#00a65a'
    barChartData.datasets[1].strokeColor = '#00a65a'
    barChartData.datasets[1].pointColor  = '#00a65a'
    var barChartOptions                  = {
      //Boolean - WhethbarCharter the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
  })

  //laporan
  tipelaporan=""
  int1=$('#laporan').val();
  switch(int1){
    case 1:tipelaporan="pemakaian";break;
    case 2:tipelaporan="pemakaian";break;
    case 3:tipelaporan="pemakaian";break;
    case 4:tipelaporan="pemakaian";break;
    default:tipelaporan="";break;
  }
  
  // var laporanurl="<?php echo base_url('c_admin/print/') ?>"+tipelaporan
  var laporanurl="<?php 
  if(isset($menuName)){
    echo base_url($menuName);
  }
  ?>"

  var laporanurlgo=laporanurl
  var set=$('#set')

  $('#th').on('click',function(){
    th=$(this).val()
    bln=$('#bln').val()
    laporanurlgo=laporanurl+'/'+th+'/'+bln
    set.attr('href',laporanurlgo)
    // alert("select")
  })

  $('#bln').on('click',function(){
    th=$('#th').val()
    bln=$(this).val()
    laporanurlgo=laporanurl+'/'+th+'/'+bln
    set.attr('href',laporanurlgo)
    // alert("select")
  })

  $('#sub').on('click',function(){
    // window.location.href=laporanurlgo;
    // alert("select")
  })
</script>



</body>
</html>
