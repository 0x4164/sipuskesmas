<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- <title>Sistem Informasi Puskesmas Ambulu</title> -->
  <title>SIPBAT</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>bower_components/morris.js/morris.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?php echo base_url("assets/") ?>https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="<?php echo base_url("assets/src/") ?>jquery.inputpicker.css"/>
  <style>
  @font-face {
    font-family:"DroidSans";
    src: url("<?php echo base_url();?>assets/fonts/DroidSans.ttf") format("truetype");
    font-family:"LatoReg";
    src: url("<?php echo base_url();?>assets/fonts/Lato-Regular.ttf") format("truetype");
  }
  </style>
</head>

<?php
//sess handle
$usr=$this->session->userdata('username');
$usrlv=$this->session->userdata('usrlv');
$usrlv_str=$this->session->userdata('usrlv_str');

// superadmin
$lv0=$usrlv==0;

// Poligigi
$lv1=$usrlv==1;

// Gudang
$lv2=$usrlv==2;

// Kepala
$lv3=$usrlv==3;

$priv=$lv0||$lv1||$lv2||$lv3;

?>
<?php ?>
<body class="hold-transition 
<?php if (!$lv2){ ?>
skin-blue
<?php }else{ ?>
skin-red
<?php } ?>
 sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="#" class="logo">
      <span class="logo-mini"><b>P</b>A</span>
      <!-- <span class="logo-lg"><b>Puskesmas</b>Ambulu</span> -->
      <span class="logo-lg"><b style="font-family:'LatoReg'">SIPBAT</b>
      <?php if (!$lv2){ ?>

<?php }else{ ?>
Gudang
<?php } ?>
      </span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        <!-- notif -->
        <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">1</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 1 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> <?php echo date('Y-m-d');?>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>
              <!-- .notif -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php
                if ($lv0){
                  ?> <img src="<?php echo base_url("assets/img/avatar/S.png") ?>" class="user-image" alt="User Image"> <?php
                }else if ($lv1){
                  ?> <img src="<?php echo base_url("assets/img/avatar/P.png") ?>" class="user-image" alt="User Image"> <?php
                }else if ($lv2){
                  ?> <img src="<?php echo base_url("assets/img/avatar/G.png") ?>" class="user-image" alt="User Image"> <?php
                }else if ($lv3){ 
                  ?> <img src="<?php echo base_url("assets/img/avatar/K.png") ?>" class="user-image" alt="User Image"> <?php
                }
              ?>
              <!-- <img src="<?php echo base_url("assets/img/avatar.jpg") ?>" class="user-image" alt="User Image"> -->
              
              <span class="hidden-xs"><?php echo $usr." - ".$usrlv_str;?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
              <?php
              if ($lv0){
                ?> <img src="<?php echo base_url("assets/img/avatar/S.png") ?>" class="img-circle" alt="User Image"> <?php
              }else if ($lv1){
                ?> <img src="<?php echo base_url("assets/img/avatar/P.png") ?>" class="img-circle" alt="User Image"> <?php
              }else if ($lv2){
                ?> <img src="<?php echo base_url("assets/img/avatar/G.png") ?>" class="img-circle" alt="User Image"> <?php
              }else if ($lv3){ 
                ?> <img src="<?php echo base_url("assets/img/avatar/K.png") ?>" class="img-circle" alt="User Image"> <?php
              }
              ?>
                <!-- <img src="<?php echo base_url("assets/img/avatar.jpg") ?>" class="img-circle" alt="User Image"> -->

                <p>
                  <!-- Nama Administrator -->
                  <?php echo $usr;?>
                  <!-- <small>Administrator</small> -->
                  <small><?php echo $usrlv_str;?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-right">
                  <a href="<?php echo base_url('c_admin/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
            
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <!-- <img src="<?php echo base_url("assets/img/avatar.jpg") ?>" class="img-circle" alt="User Image"> -->
          <?php
          if ($lv0){
            ?> <img src="<?php echo base_url("assets/img/avatar/S.png") ?>" class="img-circle" alt="User Image"> <?php
          }else if ($lv1){
            ?> <img src="<?php echo base_url("assets/img/avatar/P.png") ?>" class="img-circle" alt="User Image"> <?php
          }else if ($lv2){
            ?> <img src="<?php echo base_url("assets/img/avatar/G.png") ?>" class="img-circle" alt="User Image"> <?php
          }else if ($lv3){
            ?> <img src="<?php echo base_url("assets/img/avatar/K.png") ?>" class="img-circle" alt="User Image"> <?php
          }
          ?>
          <!-- <img src="<?php echo base_url("assets/img/avatar/P.png") ?>" class="img-circle" alt="User Image"> -->
          <?php
          
          ?>
        </div>
        <div class="pull-left info">
          <!-- <p>Admin</p> -->
          <p><?php echo $usr;?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Aktif</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU UTAMA</li>
        <?php ?>
        <?php if ($priv){
          ?>
        <li class="<?php if ($menuName=='dashboard'){echo 'active';} ?>" >
          <a href="<?php echo base_url() ?>">
            <!-- <i class="fa fa-dashboard"></i> <span>Dashboard</span> -->
            <i class="fa fa-dashboard"></i> <span>Home</span>
          </a>
        </li>

        <?php 
        }?>

        <?php 
        $m1=$menuName=="alluser";
          //if superadmin & 
          if ($lv0){
        ?>
        <li  class="treeview <?php if ($m1){echo 'active';} ?>"  class="treeview active">
          <a href="#">
            <i class="fa fa-users"></i> <span>Pengguna</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if ($m1){echo 'active';} ?>" ><a href="<?php echo base_url('alluser') ?>"><i class="fa fa-users"></i> Semua user</a></li>
          </ul>
        </li>
        <?php
          }

          // if ($lv0||$lv2||$lv3){
            
          // if ($lv0||$lv1||$lv2){
            
          //if input
          if ($lv0||$lv1||$lv2){
        //if superadmin & 
        ?>
        <li  class="treeview <?php 
        $mobat=$menuName=='obat';
        $mpemakaian=$menuName=='pemakaian';
        $mpermintaan=$menuName=='permintaan';
        $mpenerimaan=$menuName=='penerimaan';

        $mdistribusi=$menuName=='distribusi';
        $mpengudang=$menuName=='penerimaan gudang';

        // +distribusi
        if ($mobat||$mpemakaian||$mpermintaan||$mpenerimaan||$mdistribusi||$mpengudang){
          echo 'active';
        } ?>"  class="treeview active">
          <a href="#">
            <i class="fa fa-table"></i> <span>Input</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if (!$lv2){ ?>
            <li class="<?php if ($menuName=='obat'){echo 'active';} ?>" ><a href="<?php echo base_url('obat') ?>"><i class="fa fa-circle-o"></i> Obat</a></li>
            <li class="<?php if ($menuName=='pemakaian'){echo 'active';} ?>" ><a href="<?php echo base_url('pemakaian') ?>"><i class="fa fa-circle-o"></i> Pemakaian</a></li>
            <li class="<?php if ($menuName=='permintaan'){echo 'active';} ?>" ><a href="<?php echo base_url('pemakaian') ?>"><a href="<?php echo base_url('permintaan') ?>"><i class="fa fa-circle-o"></i> Permintaan</a></li>
            <li class="<?php if ($menuName=='penerimaan'){echo 'active';} ?>" ><a href="<?php echo base_url('pemakaian') ?>"><a href="<?php echo base_url('penerimaan') ?>"><i class="fa fa-circle-o"></i> Penerimaan</a></li>
            <?php } ?>
            <?php if ($lv2||$lv0){ ?>
            <li class="<?php if ($menuName=='obat'){echo 'active';} ?>" ><a href="<?php echo base_url('obat') ?>"><i class="fa fa-circle-o"></i> Obat</a></li>
            <li class="<?php if ($menuName=='penerimaan gudang'){echo 'active';} ?>" >
            <a href="<?php echo base_url('penerimaan/gudang') ?>">
            <a href="<?php echo base_url('penerimaan/gudang') ?>"><i class="fa fa-circle-o"></i> Penerimaan</a></li>
            <li class="<?php if ($mdistribusi){echo 'active';} ?>" ><a href="<?php echo base_url('distribusi') ?>"><a href="<?php echo base_url('distribusi') ?>"><i class="fa fa-circle-o"></i> Distribusi</a></li>
            <?php
              }
             ?>
          </ul>
        </li>
        <?php 
          }
          //endif input

          if ($priv){
        //if superadmin & 
        ?>
        <li  class="treeview <?php if ($menuName=='laporanKeseluruhan'||$menuName=='laporanStok'||$menuName=='laporanPemakaian' || $menuName=='laporanPenerimaan'||$menuName=='laporanPermintaan'||$menuName=='laporan'){echo 'active';} ?>" >
          <a href="#">
            <i class="fa fa-table"></i> <span> Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <?php //if (!$lv1){?>
          <?php if ($lv1||$lv0){
            //if (!$lv1){
            ?>
            <li class="<?php if ($menuName=='laporanStok'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanStok') ?>"><i class="fa fa-circle-o"></i> Stok</a></li>
            <li class="<?php if ($menuName=='laporanStokDetail'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanStokDetail') ?>"><i class="fa fa-circle-o"></i> Transaksi</a></li>
            <li class="<?php if ($menuName=='laporanPemakaian'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanPemakaian') ?>"><i class="fa fa-circle-o"></i> Pemakaian</a></li>
            <li class="<?php if ($menuName=='laporanPermintaan'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanPermintaan') ?>"><i class="fa fa-circle-o"></i> Permintaan</a></li>
            <li class="<?php if ($menuName=='laporanPenerimaan'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanPenerimaan') ?>"><i class="fa fa-circle-o"></i> Penerimaan</a></li>
            <li class="<?php if ($menuName=='laporanKeseluruhan'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanKeseluruhan') ?>"><i class="fa fa-circle-o"></i> Laporan Keseluruhan</a></li>
          <?php ?>
          <?php 
          //jika gudang
          }else if ($lv2){
          ?>
            <li class="<?php if ($menuName=='laporanStok'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanStok') ?>"><i class="fa fa-circle-o"></i> Stok</a></li>
            <li class="<?php if ($menuName=='laporanStokDetail'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanStokDetail') ?>"><i class="fa fa-circle-o"></i> Transaksi</a></li>
          <?php
          }else{
            ?>
            <li class="<?php if ($menuName=='laporanPemakaian'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanPemakaian') ?>"><i class="fa fa-circle-o"></i> Pemakaian</a></li>
            <li class="<?php if ($menuName=='laporanPermintaan'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanPermintaan') ?>"><i class="fa fa-circle-o"></i> Permintaan</a></li>
            <!-- <li class="<?php if ($menuName=='laporanKeseluruhan'){echo 'active';} ?>" ><a href="<?php echo base_url('laporanKeseluruhan') ?>"><i class="fa fa-circle-o"></i> Laporan Keseluruhan</a></li> -->
          <?php
          } ?>
          </ul>
        </li>
        <?php 
        }
        //if superadmin & 
        ?>
      </ul>
    </section>
  </aside>

