<?php
//sess handle
$usr=$this->session->userdata('username');
$usrlv=$this->session->userdata('usrlv');
$usrlv_str=$this->session->userdata('usrlv_str');

// superadmin
$lv0=$usrlv==0;
// Poligigi
$lv1=$usrlv==1;
// Gudang
$lv2=$usrlv==2;
// Kepala
$lv3=$usrlv==3;

$priv=$lv0||$lv1||$lv2||$lv3;

?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Laporan Permintaan
        <small>Poli Gigi Puskesmas Ambulu</small>
      </h1>
    </section>
    <section class="content">
      
      <br>
      <div class="row">
        <div class="col-xs-12">
          
          <?php if(!$lv1){ ?>
          <!-- graf -->
            <!-- BAR CHART -->
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Grafik permintaan obat</h3>
                <div class="box-tools pull-right">
                  <?php  ?>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
              <!-- indicator -->
                  <?php 
                  //iter x button
                  //rgba(210, 214, 222, 1) #d2ded6
                  $colors=array();
                  $colors[]='#00c0ef';
                  $colors[]='#00a65a';
                  $colors[]='#f39c12';
                  $colors[]='#f56954';
                  $colors[]='#d2d6de';
                  $colors[]='#ff851b';

                  $n=0;
                  foreach($topobat as $r1){
                    ?>
                    <button class="btn" style="background-color:<?php echo $colors[$n];?>;color:#ffffcc"><?php echo $r1->nama_obat;?></button>
                    <?php 
                    $n++;
                  }
                  ?>
                  <!-- <button class="btn" style="background-color:#00a65a">o2</button> -->
              <!-- .indicator -->
              <?php //echo $year; 
              // var_dump($grtahun);
              ?>
                <div class="chart">
                  <canvas id="barChart2" style="height:400px"></canvas>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          <!-- .graf -->
          <?php 
          } ?>
          
          <div class="box box-info
            <?php if(!$lv1){?>
              collapsed-box
            <?php } ?>
           ">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel</h3>
              <div class="box-tools pull-right">
                <?php  ?>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Tgl, bln, thn</th>
                  <th>Nama bahan/obat</th>
                  <th>Jumlah Permintaan</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($result as $r) { ?>
                <tr>
                  <td><?php echo $r['id'] ?></td>
                  <td><?php echo $r['tgl_permintaan'] ?></td>
                  <td><?php echo $r['nama_obat'] ?></td>
                  <td><?php echo $r['jumlah'] ?></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>

                <!-- input tanggal -->
                <input id="laporan" type="hidden" name="laporan" value="1">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Tahun</label>
                      <select class="form-control" name="th" id="th">
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Bulan</label>
                      <select class="form-control" name="bln" id="bln">
                        <?php for($i=1;$i<=12;$i++){
                          ?>
                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                          <?php 
                        }?>
                      </select>
                    </div>
                    <div class="form-group">
                      <!-- <button id="sub" class="btn btn-success">Set</button> -->
                      <a id="set" class="btn btn-success" href="">Set</a>
                    </div>
                  </div>
                </div>
                <!-- .input tanggal -->

              <a id="laporanurl" href="<?php echo base_url('c_admin/print/permintaan/'.$th.'/'.$bln) ?>" target="_blank" class="btn btn-success" ><span class="fa fa-print"></span> Print</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  