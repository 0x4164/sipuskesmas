<?php
//sess handle
$usr=$this->session->userdata('username');
$usrlv=$this->session->userdata('usrlv');
$usrlv_str=$this->session->userdata('usrlv_str');

// superadmin
$lv0=$usrlv==0;

// Poligigi
$lv1=$usrlv==1;

// Gudang
$lv2=$usrlv==2;

// Kepala
$lv3=$usrlv==3;

$priv=$lv0||$lv1||$lv2||$lv3;

?>
<style>
/* custom button clr
https://www.jquery-az.com/bootstrap-button-9-demos-of-custom-color-size-dropdown-navbar-and-more/
 */
  .btn-default1{
    background-color: #68889E;
    color:#FFF;
    border-color: #2F3E48;
  }
    
  .btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open .dropdown-toggle.btn-default {  
    background-color: #2F3E48;
    color:#FFF;
    border-color: #31347B;
  }
</style>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Laporan Stok Obat detail
        <small>Poli Gigi Puskesmas Ambulu</small>
      </h1>
    </section>
    <section class="content">
      
      <br>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
            
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>id</th>
                  <th>Batch</th>
                  <th>Nama bahan/obat</th>
                  <th>Jumlah Transaksi</th>
                  <th>Tipe Transaksi</th>
                  <th>Waktu</th>
                </tr>
                </thead>
                <tbody>
                <!-- id
batch
noobat
jml
tipetrans
waktutrans -->
                <?php  $dateExp = date('Y-m-d', strtotime("+60 days")); $today= date('Y-m-d'); foreach ($result as $r) { ?>
                <tr>
                  <td><?php echo $r['id'] ?></td>
                  <td><?php echo $r['batch'] ?></td>
                  <td><?php echo $r['nama_obat'] ?></td>
                  <?php ?>
                  <?php if($gudang) {?>
                    <td><?php echo $r['jml'] ?></td>
                  <?php }else{?>
                    <td><?php echo $r['jml'] ?></td>
                  <?php }?>
                  <?php //echo $lv;?>
                  <td><?php 
                  if($r['tipetrans']==1){
                    echo "Distribusi"; 
                  }else{
                    echo "Terima"; 
                  }
                  ?></td>
                  <td><?php echo $r['waktutrans'] ?></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
              <a href="<?php echo base_url('c_admin/print/stok') ?>" target="_blank" class="btn btn-success" ><span class="fa fa-print"></span> Print</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  