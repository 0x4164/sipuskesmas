<?php

$gudang=true;

?>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Laporan
        <small>Poli Gigi Puskesmas Ambulu</small>
      </h1>
    </section>
    <section class="content">
      <div class="row">
      </div>
      <br>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama bahan/obat</th>
                  <th>Jumlah Awal</th>
                  <th>Stok sisa</th>
                  <th>Jumlah</th>
                  <th>Tgl, bln, thn</th>
                  <th>Jenis</th>
                  <th>Kondisi</th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                  <?php 
                  $dateExp = date('Y-m-d', strtotime("+60 days"));
                  $today= date('Y-m-d');
                  foreach ($obat as $r) {
                    $r['trsize2']=$r['trsize']==0?1:$r['trsize'];
                     ?>
                    <td rowspan="<?php echo $r['trsize2'];?>"><?php echo $r['nx'];?></td>
                    <td rowspan="<?php echo $r['trsize2'];?>">
                      <?php echo $r['obat']->nama_obat; ?><br>
                      no.obat:<?php echo $r['obat']->no_obat; ?><br>
                      batch:<?php echo $r['obat']->batch_no; ?>
                    </td>
                    <td rowspan="<?php echo $r['trsize2'];?>"><?php echo $r['obat']->jumlah_stok; ?></td>
                    <?php if($gudang){ ?>
                      <td rowspan="<?php echo $r['trsize2'];?>"><?php echo $r['obat']->stokgudang; ?></td>
                    <?php }else{ ?>
                      <td rowspan="<?php echo $r['trsize2'];?>"><?php echo $r['stok_obat'] ?></td>
                    <?php } ?>
                    <?php
                    $n=1;
                    if($r['trsize']==0){
                      ?>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>-</td>
                      <!-- <td>-</td> -->
                      </tr>
                      <tr>
                      <?
                      // continue;
                    }
                    foreach ($r['obattrans'] as $trans) {
                      ?>
                        <td>
                          <?php echo $trans->jml; ?><br>
                          <?php //echo $trans->jml; ?>
                      </td>
                        <td><?php echo $trans->waktutrans; ?></td>
                        <td><?php echo $trans->tipe; ?></td>
                        

                        <td style="text-align: center"><span class="btn btn-success"><?php 
                          // $this->M_stok->tglExp($r['no_obat'])->exp_date;
                          // $expObat = $this->M_stok->tglExp($r['no_obat'])->exp_date;
                          // $expStok = $this->M_stok->tglExp($r['no_obat'])->jumlah_stok;
                          $expObat = $r['obat']->exp_date;
                          $expStok = $r['obat']->stokgudang;
                          // $expStok = $this->M_stok->tglExp($r['no_obat'])->jumlah_stok;
                        if($today>$expObat){
                          echo $expStok.' obat Kadaluarsa';
                        }elseif ($dateExp>$expObat){
                          echo $expStok.' obat Hampir Kadaluarsa';
                        }else{
                          echo'Baik';
                        }?></span></td>
                      </tr>
                      <?php 
                      if($n!=1){
                        ?>
                        <tr>
                        <?php
                      }
                      $n++;
                      ?>

                      <?php
                    } ?>
                  <?php ?>
                  
                </tr>
              <?php } ?>
                </tbody>
              </table>
              <!-- input tanggal -->
              <input id="laporan" type="hidden" name="laporan" value="1">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Tahun</label>
                      <select class="form-control" name="th" id="th">
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Bulan</label>
                      <select class="form-control" name="bln" id="bln">
                        <?php for($i=1;$i<=12;$i++){
                          ?>
                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                          <?php 
                        } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <!-- <button id="sub" class="btn btn-success">Set</button> -->
                      <a id="set" class="btn btn-success" href="">Set</a>
                    </div>
                  </div>
                </div>
                <!-- .input tanggal -->
                
              <a id="laporanurl" href="<?php echo base_url('c_admin/print/keseluruhan'.$th.'/'.$bln) ?>" target="_blank" class="btn btn-success" ><span class="fa fa-print"></span> Print</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  