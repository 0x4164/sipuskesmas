<?php 
/**
 * 
 * previous develper suck :(
 * 
 */
class M_laporan extends CI_Model
{
	
	function readPemakaian($th=2019,$bln=0){
		$q1="";
		if($th!=0){
			$q1="where year(tgl_pemakaian)=$th";
			if($bln!=0){
				$q1=$q1." and month(tgl_pemakaian)=$bln";
			}
		}

		$query = $this->db->query("SELECT 
		pemakaian.id, stok_obat.no_obat, jumlah_pemakaian, obat.nama_obat, tgl_pemakaian 
		FROM pemakaian 
		INNER JOIN (stok_obat 
		inner join obat on stok_obat.no_obat=obat.no_obat) 
		on pemakaian.batch_no=stok_obat.batch_no
		$q1
		");
		return $query;
		// INNER JOIN (stok_obat inner join obat on stok_obat.no_obat=obat.id_obat) on pemakaian.batch_no=stok_obat.batch_no
	}

	function readPenerimaan($th=2019,$bln=0){
		$q1="";
		if($th!=0){
			$q1="where year(tgl_penerimaan)=$th";
			if($bln!=0){
				$q1=$q1." and month(tgl_penerimaan)=$bln";
			}
		}

		$query = $this->db->query("SELECT penerimaan.id, obat.nama_obat, jumlah, tgl_penerimaan 
		FROM penerimaan 
		INNER JOIN obat on penerimaan.no_obat=obat.no_obat
		$q1
		");
		return $query;
		// INNER JOIN obat on penerimaan.no_obat=obat.id_obat

	}

	function readPermintaan($th=2019,$bln=0){
		$q1="";
		if($th!=0){
			$q1="where year(tgl_permintaan)=$th";
			if($bln!=0){
				$q1=$q1." and month(tgl_permintaan)=$bln";
			}
		}

		$query = $this->db->query("SELECT permintaan.id, obat.nama_obat, jumlah, tgl_permintaan 
		FROM permintaan 
		INNER JOIN obat on permintaan.no_obat=obat.no_obat
		$q1
		");
		return $query;
		// INNER JOIN obat on permintaan.no_obat=obat.id_obat
	}

	function readLaporan($gudang=false,$th=0,$bln=0){
		// return $query = $this->db->query("SELECT * FROM laporan, obat 
		// WHERE laporan.no_obat = obat.no_obat");
		$query="";
		$q1="";
		if($th!=0){
			$q1="where year(tgl_laporan)=$th";
			if($bln!=0){
				$q1=$q1." and month(tgl_laporan)=$bln";
			}
		}

		if($gudang){
			//work

			$q="SELECT laporan.*,obat.*,so.* FROM laporan
			inner join obat on laporan.no_obat = obat.no_obat
			inner join stok_obat so on so.no_obat = laporan.no_obat
			$q1";
			$query = $this->db->query($q);

		}else{
			$q="SELECT laporan.*,obat.*,so.* FROM laporan
			inner join obat on laporan.no_obat = obat.no_obat
			inner join stok_obat so on so.no_obat = laporan.no_obat
			$q1";
			$query = $this->db->query($q);
		}

		return $query;

		// return $query = $this->db->query("SELECT laporan.*,obat.*,so.* FROM laporan
		// inner join obat on laporan.no_obat = obat.no_obat
		// inner join stok_obat so on so.no_obat = laporan.no_obat");
	}

	function getstok(){
		$this->db->select('so.*,o.nama_obat');
		$this->db->from('stok_obat so');
		$this->db->join('obat o','o.no_obat=so.no_obat');
		
		$q=$this->db->get();

		return $q;
	}

	function gettrans($batch=0){
		$q=null;
		$this->db->select('*');
		$this->db->from('obat_trans ot');
		$this->db->join('obat o','o.no_obat=ot.noobat');
		$this->db->where('batch',$batch);
		
		$q=$this->db->get();

		return $q;
	}

	function readStok($gudang=false){
		// $q=$gudang?;
		$q="";
		if($gudang){
			// $q="SELECT so.*,o.nama_obat,pe.jumlah_pemakaian as jmlpemakaian,pen.jumlah as jmlpenerimaan,per.jumlah as jmlpermintaan FROM stok_obat so
			// INNER JOIN obat o on so.no_obat=o.no_obat
			// inner join pemakaian pe on so.batch_no=pe.batch_no
			// inner join penerimaan pen on so.batch_no=pen.batch_no
			// inner join permintaan per on so.no_obat=per.no_obat";
			$q="SELECT so.*,o.nama_obat FROM stok_obat so
			INNER JOIN obat o on so.no_obat=o.no_obat";
		}else{
			$q="SELECT * FROM stok_obat 
			INNER JOIN obat on stok_obat.no_obat=obat.no_obat
			";
		}
		$query = $this->db->query($q);
		// echo $this->db->last_query();
		// exit();
		return $query;
		// INNER JOIN obat on stok_obat.no_obat=obat.id_obat
	}

	function readStokDetail($gudang=false){
		$this->db->select('ot.*,o.nama_obat');
		$this->db->from('obat_trans ot');
		$this->db->join('obat o','o.no_obat=ot.noobat');
		$q=$this->db->get();
		return $q;
	}
	
	function readStokExp($date1, $date2){
		return $query = $this->db->query("SELECT * FROM stok_obat INNER JOIN obat on stok_obat.no_obat=obat.id_obat WHERE (exp_date BETWEEN '$date1' AND '$date2')");
	}


	/*
	 SELECT so.*,pem.*,sum(jumlah_pemakaian) as tot FROM pemakaian pem
	inner join stok_obat so on so.batch_no=pem.batch_no
	GROUP by so.no_obat
	 */
	// SELECT * FROM `pemakaian` where MONTH(tgl_pemakaian) = 4

	//0x4164

	function pemakaian_pertahun($thn=1,$limit=false){
		$l="";
		if($limit){
			$l="limit 5";
		}
		$q="SELECT o.*,so.*,pem.*,sum(jumlah_pemakaian) as tot FROM pemakaian pem
		inner join stok_obat so on so.batch_no=pem.batch_no
		inner join obat o on o.no_obat=so.no_obat
		where year(tgl_pemakaian)=$thn
		GROUP by so.no_obat 
		order by tot desc ".$l;
		$qu=$this->db->query($q);

		return $qu;
	}

	function penerimaan_pertahun($thn=1,$limit=false){
		$l="";
		if($limit){
			$l="limit 5";
		}
		$q="SELECT o.*,so.*,pen.*,sum(jumlah) as totpen FROM penerimaan pen
		inner join stok_obat so on so.batch_no=pen.batch_no
		inner join obat o on o.no_obat=so.no_obat
		where year(tgl_penerimaan)=$thn
		GROUP by so.no_obat 
		order by totpen desc ".$l;
		$qu=$this->db->query($q);

		return $qu;
	}

	function permintaan_pertahun($thn=1,$limit=false){
		$l="";
		if($limit){
			$l="limit 5";
		}

		//beda
		$q="SELECT o.*,per.*,sum(jumlah) as totpen FROM permintaan per
		inner join obat o on o.no_obat=per.no_obat
		where year(tgl_permintaan)=$thn
		GROUP by per.no_obat 
		order by totpen desc ".$l;
		$qu=$this->db->query($q);

		return $qu;
	}

	function pemakaian_bybulan($bln=1){
		$q="SELECT so.*,pem.*,sum(jumlah_pemakaian) as tot FROM pemakaian pem
		inner join stok_obat so on so.batch_no=pem.batch_no
		where month(tgl_pemakaian)=$bln
		GROUP by so.no_obat";
		$qu=$this->db->query($q);

		return $qu;
	}

	//show perbulan (12 bulan) dlm 1 tahun tertentu
	//pemakaian
	function pemakaian_byobat($idobat=1,$thn=2019){
		//month(tgl_pemakaian) as blnpem --- bulan pemakaian
		$q="SELECT month(tgl_pemakaian) as blndata,so.*,pem.*,sum(jumlah_pemakaian) as tot 
		FROM pemakaian pem
		inner join stok_obat so on so.batch_no=pem.batch_no
		where so.no_obat=$idobat and year(tgl_pemakaian)=$thn
		GROUP by month(tgl_pemakaian)";
		
		$qu=$this->db->query($q);

		return $qu;
	}

	//show perbulan (12 bulan) dlm 1 tahun tertentu
	//penerimaan
	function penerimaan_byobat($idobat=1,$thn=2019,$bln=1){
		//month(tgl_pemakaian) as blnpem --- bulan pemakaian
		// $q1=
		// if($bln>1){
		// 	$q1="and month(tgl_penerimaan)=$bln";
		// }
		$q="SELECT month(tgl_penerimaan) as blndata,so.*,pen.*,sum(jumlah) as tot FROM penerimaan pen
		inner join stok_obat so on so.batch_no=pen.batch_no
		where so.no_obat=$idobat and year(tgl_penerimaan)=$thn
		GROUP by month(tgl_penerimaan)";
		$qu=$this->db->query($q);

		return $qu;
	}

	//show perbulan (12 bulan) dlm 1 tahun tertentu
	//penerimaan
	function permintaan_byobat($idobat=1,$thn=2019){
		//month(tgl_pemakaian) as blnpem --- bulan pemakaian
		$q="SELECT month(tgl_permintaan) as blndata,per.*,sum(jumlah) as tot FROM permintaan per
		where per.no_obat=$idobat and year(tgl_permintaan)=$thn
		GROUP by month(tgl_permintaan)";
		$qu=$this->db->query($q);

		return $qu;
	}
}
 ?>