<?php
class M_user extends CI_Model{
	function __construct()
	{
		# code..
		$table = 'user';
		$this->load->database();
		// $this->load->model('M_stok');
		// $this->load->model('M_obat');
	}

	function create($data=array()){
		$this->db->insert('user',$data);
	}

	function getalluser(){
		$this->db->select('*');
		$this->db->from('user');
		$q=$this->db->get();
		return $q;
	}

	function getalluserlevel(){
		$this->db->select('*');
		$this->db->from('user_level');
		$q=$this->db->get();
		return $q;
	}

	function getuser($id=1){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id',$id);
		$q=$this->db->get();
		return $q;
	}

	function update($id=0,$data=array()){
		$this->db->where('id',$id);
		$q=$this->db->update('user',$data);
		// echo $this->db->last_query();
		// $this->db->get();
		return $q;
	}

	function delete($id=0,$data=array()){
		$this->db->where('id',$id);
		$q=$this->db->delete('user');
		// $q=$this->db->get();
		return $q;
	}
}
