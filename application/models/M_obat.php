<?php
class M_obat extends CI_Model
{
  function __construct()
  {
    # code..
    $table = 'users';
    $this->load->database();
  }

  function createObat(){
      $no_obat = $this->input->post('no_obat');
      $nama_obat = strtolower($this->input->post('nama_obat'));
      $tipe = $this->input->post('tipe');
      $data = array(
        "no_obat"   => $this->input->post('no_obat'),
        "nama_obat" => $this->input->post('nama_obat'),
        "tipe"      => $this->input->post('tipe')
      );
      $this->db->insert("obat",$data);
	}
	
	function getObat($id=FALSE){

		if(!$id){
			$query = $this->db->query('select * from obat');
			return $query->result();
		}
	}

  function read()
  {
    return $query = $this->db->query("SELECT * FROM obat");
  }

  function getLastId(){
    $last_row=$this->db->select('no_obat')->order_by('id_obat',"desc")->limit(1)->get('obat')->row();
    // echo $this->db->last_query();
    if ($last_row == null)
    return 0;
    else
    return $last_row->no_obat;
   
	}
	
	function getNoObatByNama($nama_obat){
		$no_obat_array = $this->db->select('no_obat')->where('nama_obat',$nama_obat)->get("obat")->result_array();
		$no_obat = $no_obat_array[0]["no_obat"];
		return $no_obat;
  }
  
  function get_allobattipe(){
    $this->db->select('*');
    $this->db->from('obat_tipe');
    $q=$this->db->get();
    return $q;
  }
  // SELECT * FROM `stok_obat`

  function get_maxbatch(){
    $this->db->select('max(batch_no) as maxbatch');
    $this->db->from('stok_obat');
    $q=$this->db->get();
    return $q;
  }
  /**
   * id=id stok obat
   * adata= add data
   * udata= update data
   * 
   * arah = 1 ?, maka distribusi ke poligigi, jika tidak maka ke gudang dulu
   * 
   *  */
  function distribusi($id=0,$adata=[],$udata=[],$arah=1){
    //add di obat_trans
    
    
    //update di stok_obat
    if($arah==1){
      $q1=$this->db->insert('obat_trans',$adata);
      $this->db->set('stokpoligigi','stokpoligigi+'.$adata['jml'],false);
      $this->db->set('stokgudang','stokgudang-'.$adata['jml'],false);

      $this->db->where('id',$id);
      $q2=$this->db->update('stok_obat',$udata);
      // echo $this->db->last_query();
      $ret=array($q1,$q2,$id,$adata,$udata);
    }else{
      $q1=$this->db->insert('obat_trans',$adata[0]);
      $q2=$this->db->insert('stok_obat',$adata[1]);
      $ret=[$q1,$q2];
      // $this->db->set('stokgudang',$adata['jml'],false);
    }
    
    return $ret;
  }

}
