<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'C_admin/index';
$route['login'] = 'Auth/login';
$route['obat'] = 'C_admin/obat';
$route['distribusi'] = 'C_admin/distribusi';
$route['dbdistribusi'] = 'C_admin/dbdistribusi';
$route['pemakaian'] = 'C_admin/pemakaian';
$route['permintaan'] = 'C_admin/permintaan';
$route['penerimaan'] = 'C_admin/penerimaan';
$route['penerimaan/(:any)'] = 'C_admin/penerimaan/$1';
// $route['stock']='c_admin/index/stock';
$route['stock']='c_admin/laporanStok';
$route['expired']='c_admin/index/expired';

$route['laporanPemakaian'] = 'C_admin/laporanPemakaian';
$route['laporanPemakaian/(:any)'] = 'C_admin/laporanPemakaian/$1';
$route['laporanPemakaian/(:any)/(:any)'] = 'C_admin/laporanPemakaian/$1/$2';

$route['laporanPenerimaan'] = 'C_admin/laporanPenerimaan';
$route['laporanPenerimaan/(:any)'] = 'C_admin/laporanPenerimaan/$1';
$route['laporanPenerimaan/(:any)/(:any)'] = 'C_admin/laporanPenerimaan/$1/$2';

$route['laporanPermintaan'] = 'C_admin/laporanPermintaan';
$route['laporanPermintaan/(:any)'] = 'C_admin/laporanPermintaan/$1';
$route['laporanPermintaan/(:any)/(:any)'] = 'C_admin/laporanPermintaan/$1/$2';

$route['laporanKeseluruhan'] = 'C_admin/laporanKeseluruhan';
$route['laporanKeseluruhanGudang'] = 'C_admin/laporanKeseluruhanGudang';
$route['laporanKeseluruhanGudang/(:any)'] = 'C_admin/laporanKeseluruhanGudang/$1';
$route['laporanKeseluruhanGudang/(:any)/(:any)/(:any)'] = 'C_admin/laporanKeseluruhanGudang/$1/$2/$3';
$route['laporanKeseluruhan/(:any)'] = 'C_admin/laporanKeseluruhan/$1';
$route['laporanKeseluruhan/(:any)/(:any)'] = 'C_admin/laporanKeseluruhan/$1/$2';

$route['laporanStok'] = 'C_admin/laporanStok';
$route['laporanStokDetail'] = 'C_admin/laporanStokDetail';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//user
$route['alluser'] = 'c_admin/alluser';
$route['user/ajaxgetuser'] = 'c_admin/ajax_singleuser';
$route['user/ajaxgetuser/(:any)'] = 'c_admin/ajax_singleuser/$1';
$route['c_admin/dbuser_delete/(:any)'] = 'c_admin/dbuser_delete/$1';

$route['user/edit'] = 'c_admin/user_edit';
$route['user/tambah'] = 'c_admin/dbuser_insert';
$route['user/dbupdate'] = 'c_admin/dbuser_update';
$route['user/delete/(:any)'] = 'c_admin/dbuser_delete/$1';