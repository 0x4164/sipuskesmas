-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 06, 2019 at 01:39 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `puskesmas`
--

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE `laporan` (
  `id_laporan` int(11) NOT NULL,
  `no_obat` varchar(512) NOT NULL,
  `tgl_laporan` varchar(512) NOT NULL,
  `jumlah_minta` int(11) NOT NULL,
  `jumlah_terima` int(11) NOT NULL,
  `jumlah_pakai` int(11) NOT NULL,
  `stok_obat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan`
--

INSERT INTO `laporan` (`id_laporan`, `no_obat`, `tgl_laporan`, `jumlah_minta`, `jumlah_terima`, `jumlah_pakai`, `stok_obat`) VALUES
(1, '146', '2019-01-15', 1, 1, 0, 1),
(2, '315', '2019-01-15', 1, 1, 1, 0),
(3, '409', '2019-01-15', 1, 1, 1, 0),
(4, '617', '2019-01-15', 0, 45, 0, 45),
(5, '1103', '2019-01-15', 1, 1, 0, 1),
(6, '1105', '2019-01-15', 1, 1, 1, 0),
(7, '1113', '2019-01-15', 0, 8, 8, 0),
(8, '1217', '2019-01-15', 0, 24, 2, 22),
(9, '1305', '2019-01-15', 0, 8, 0, 8),
(10, '1305', '2019-01-15', 0, 40, 10, 30),
(11, '1831', '2019-01-15', 0, 6, 2, 4),
(12, '2501', '2019-01-15', 0, 3, 2, 1),
(13, '1', '2019-02-15', 0, 100, 100, 0),
(14, '146', '2019-02-15', 0, 1, 1, 0),
(15, '617', '2019-02-15', 0, 0, 0, 45),
(16, '808', '2019-02-15', 1, 1, 0, 1),
(17, '1103', '2019-02-15', 0, 1, 1, 0),
(18, '1305', '2019-02-15', 50, 30, 32, 0),
(19, '2501', '2019-02-15', 1, 1, 0, 1),
(20, '315', '2019-03-15', 1, 1, 0, 1),
(21, '320', '2019-03-15', 2, 2, 0, 2),
(22, '321', '2019-03-15', 2, 2, 0, 2),
(23, '322', '2019-03-15', 2, 2, 0, 2),
(24, '1113', '2019-03-15', 0, 0, 4, 0),
(25, '1217', '2019-03-15', 0, 0, 7, 0),
(26, '1305', '2019-03-15', 0, 0, 12, 0),
(27, '2501', '2019-03-15', 0, 0, 1, 0),
(28, '320', '2019-04-15', 1, 0, 0, 0),
(29, '321', '2019-04-15', 1, 0, 0, 0),
(30, '322', '2019-04-15', 1, 0, 0, 0),
(31, '509', '2019-04-15', 1, 1, 0, 0),
(32, '617', '2019-04-15', 3, 0, 0, 0),
(33, '1217', '2019-04-15', 5, 0, 0, 0),
(34, '1305', '2019-04-15', 26, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `id_obat` int(11) NOT NULL,
  `no_obat` int(11) NOT NULL,
  `nama_obat` varchar(67) NOT NULL,
  `tipe` varchar(67) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`id_obat`, `no_obat`, `nama_obat`, `tipe`) VALUES
(1, 105, 'Air Raksa Dental Use', '1'),
(2, 138, 'Aquadest steril 1000 ml', '1'),
(3, 146, 'Aseptic Gel + Disp 500 ml ', '1'),
(4, 201, 'Bahan Tumpatan Sementara/caviton ', '1'),
(5, 315, 'Chlorhexidine 0,2%/ ASEPTAN', '1'),
(6, 316, 'Chlorhexidine 4%/ ONE SCROB', '1'),
(7, 320, 'Composit Light Curing A2', '7'),
(8, 321, 'Composit Light Curing A3', '7'),
(9, 322, 'Composit Light Curing A3,5', '7'),
(10, 325, 'Cresopen Dentin Conditiner / Mini Pack', '1'),
(11, 409, 'Devitalisasi Pasta', '1'),
(12, 417, 'Difenhidramina HCL inj 10 mg', '2'),
(13, 504, 'Epinefrina HCL inj 0,1%-1ml', '2'),
(14, 509, 'Etanol 70% - 1000ml', '1'),
(15, 513, 'Etil Klorida semprot', '1'),
(16, 514, 'Eugenol Cairan 10ml', '1'),
(17, 617, 'Fluoride Sediaan Topikal/ Clinpro W ', '1'),
(18, 710, 'Glass Ionomer Cement', '4'),
(19, 808, 'Hidrogen Peroksida / H202', '1'),
(20, 1103, 'Kalsium Hidroksida pasta', '7'),
(21, 1105, 'Kapas Pembalut abs 250 gram', '5'),
(22, 1111, 'Kasa Pembalut 4x10cm ', '5'),
(23, 1113, 'Kasa Pembalut Hidrofil 4mx15cm ', '5'),
(24, 1216, 'Lidocain 2% injeksi', '2'),
(25, 1217, 'Lidocain Comp 2% injeksi', '2'),
(26, 1305, 'Masker ear loop / karet', '3'),
(27, 1305, 'Masker plus ear loop /jilbab ', '3'),
(28, 1306, 'Masker Tie On/ Tali ', '3'),
(29, 1336, 'Monoklorkamfer Mentol Cair', '1'),
(30, 1337, 'Mummifying Pasta', '1'),
(31, 1628, 'Polikresulen (Metakresolsulfonat)', '1'),
(32, 1812, 'Semen seng fosfat ', '4'),
(33, 1824, 'Silver Amalgam serbuk 65-75%', '1'),
(34, 1831, 'Spons Gelatin cubcles 1x1x1cm', '3'),
(35, 1903, 'Temporary Stopping Fletcher', '4'),
(36, 1919, 'Topical Anastesi Gel/Prime Gel', '8'),
(37, 1926, 'Trikresol Formalin  (Forinokresol)', '1'),
(38, 2101, 'Vaselin Alba ', '1'),
(39, 2501, 'Yodium Povidon 10%- 30ml', '1'),
(40, 1, 'Safe Glove ', '8'),
(41, 2, 'Ething + Bonding ', '8'),
(42, 3, 'Xylocain Spray', '8');

-- --------------------------------------------------------

--
-- Table structure for table `obat_tipe`
--

CREATE TABLE `obat_tipe` (
  `id` int(11) NOT NULL,
  `tipe` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat_tipe`
--

INSERT INTO `obat_tipe` (`id`, `tipe`) VALUES
(1, 'Botol'),
(2, 'Ampul'),
(3, 'Biji'),
(4, 'Set'),
(5, 'Rol'),
(6, 'Cair'),
(7, 'tube'),
(8, 'lain');

-- --------------------------------------------------------

--
-- Table structure for table `obat_trans`
--

CREATE TABLE `obat_trans` (
  `id` int(11) NOT NULL,
  `batch` int(11) DEFAULT NULL,
  `noobat` int(11) DEFAULT NULL,
  `jml` int(11) DEFAULT NULL,
  `tipetrans` int(11) DEFAULT NULL,
  `waktutrans` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='transaksi obat';

--
-- Dumping data for table `obat_trans`
--

INSERT INTO `obat_trans` (`id`, `batch`, `noobat`, `jml`, `tipetrans`, `waktutrans`) VALUES
(1, 1, 146, 1, 1, '2019-04-06 05:53:23'),
(2, 1, 146, 1, 1, '2019-04-06 05:57:43'),
(3, 14, 146, 1, 1, '2019-04-06 06:01:25'),
(4, 2, 315, 1, 1, '2019-04-06 06:04:47'),
(5, 22, 315, 1, 1, '2019-04-06 06:05:13'),
(6, 22, 315, 1, 1, '2019-04-06 06:06:03'),
(7, 19, 320, 1, 1, '2019-04-06 06:08:00'),
(8, 19, 320, 1, 1, '2019-04-06 06:09:51'),
(9, 20, 321, 1, 1, '2019-04-06 06:11:21'),
(10, 20, 321, 1, 1, '2019-04-06 06:11:40'),
(11, 21, 322, 1, 1, '2019-04-06 06:13:05');

-- --------------------------------------------------------

--
-- Table structure for table `obat_transtipe`
--

CREATE TABLE `obat_transtipe` (
  `id` int(11) NOT NULL,
  `tipetransaksi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tipe transaksi obat';

--
-- Dumping data for table `obat_transtipe`
--

INSERT INTO `obat_transtipe` (`id`, `tipetransaksi`) VALUES
(1, 'distribusi gudang ke poligigi');

-- --------------------------------------------------------

--
-- Table structure for table `pemakaian`
--

CREATE TABLE `pemakaian` (
  `id` int(11) NOT NULL,
  `no_pemakaian` int(11) NOT NULL,
  `batch_no` int(11) NOT NULL,
  `jumlah_pemakaian` int(11) NOT NULL,
  `tgl_pemakaian` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemakaian`
--

INSERT INTO `pemakaian` (`id`, `no_pemakaian`, `batch_no`, `jumlah_pemakaian`, `tgl_pemakaian`) VALUES
(1, 315, 2, 1, '2019-01-15'),
(2, 409, 3, 1, '2019-01-15'),
(3, 617, 4, 45, '2019-01-15'),
(4, 1105, 6, 1, '2019-01-15'),
(5, 1113, 7, 8, '2019-01-15'),
(6, 1217, 8, 2, '2019-01-15'),
(7, 1305, 9, 10, '2019-01-15'),
(8, 1831, 11, 2, '2019-01-15'),
(9, 2501, 12, 2, '2019-01-15'),
(10, 1, 13, 100, '2019-01-15'),
(11, 146, 14, 1, '2019-02-15'),
(12, 1103, 16, 1, '2019-02-15'),
(13, 1305, 17, 1, '2019-02-15'),
(14, 1113, 7, 4, '2019-03-15'),
(15, 1217, 8, 7, '2019-03-15'),
(16, 1305, 10, 12, '2019-03-15'),
(17, 2501, 18, 1, '2019-03-15'),
(18, 320, 19, 1, '2019-04-15'),
(19, 321, 20, 1, '2019-04-15'),
(20, 322, 21, 1, '2019-04-15'),
(21, 509, 23, 1, '2019-04-15'),
(22, 617, 4, 3, '2019-04-15'),
(23, 1217, 8, 5, '2019-04-15'),
(24, 1305, 9, 26, '2019-04-15'),
(25, 1217, 8, 5, '2019-04-15'),
(26, 1305, 9, 26, '2019-04-15');

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan`
--

CREATE TABLE `penerimaan` (
  `id` int(11) NOT NULL,
  `batch_no` int(11) NOT NULL,
  `no_obat` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tgl_penerimaan` date NOT NULL,
  `exp_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penerimaan`
--

INSERT INTO `penerimaan` (`id`, `batch_no`, `no_obat`, `jumlah`, `tgl_penerimaan`, `exp_date`) VALUES
(1, 1, 146, 1, '2019-01-15', '2019-12-12'),
(2, 2, 315, 1, '2019-01-15', '2019-12-12'),
(3, 3, 409, 1, '2019-01-15', '2019-12-12'),
(4, 4, 617, 45, '2019-01-15', '2019-12-12'),
(5, 5, 1103, 1, '2019-01-15', '2019-12-12'),
(6, 6, 1105, 1, '2019-01-15', '2019-12-12'),
(7, 7, 1113, 8, '2019-01-15', '2019-12-12'),
(8, 8, 1217, 24, '2019-01-15', '2019-12-12'),
(9, 9, 1305, 8, '2019-01-15', '2019-12-12'),
(10, 10, 1305, 40, '2019-01-15', '2019-12-12'),
(11, 11, 1831, 6, '2019-01-15', '2019-12-12'),
(12, 12, 2501, 3, '2019-01-15', '2019-12-12'),
(13, 13, 1, 100, '2019-01-15', '2019-12-12'),
(14, 14, 146, 1, '2019-02-15', '2019-12-12'),
(15, 15, 808, 1, '2019-02-15', '2019-12-12'),
(16, 16, 1103, 1, '2019-02-15', '2019-12-12'),
(17, 17, 1305, 30, '2019-02-15', '2019-12-12'),
(18, 18, 2501, 1, '2019-02-15', '2019-12-12'),
(19, 19, 320, 2, '2019-03-15', '2019-12-12'),
(20, 20, 321, 2, '2019-03-15', '2019-12-12'),
(21, 21, 322, 2, '2019-03-15', '2019-12-12'),
(22, 22, 315, 1, '2019-03-15', '2019-12-12'),
(23, 23, 509, 1, '2019-04-15', '2019-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `permintaan`
--

CREATE TABLE `permintaan` (
  `id` int(11) NOT NULL,
  `no_permintaan` int(11) NOT NULL,
  `no_obat` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tgl_permintaan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permintaan`
--

INSERT INTO `permintaan` (`id`, `no_permintaan`, `no_obat`, `jumlah`, `tgl_permintaan`) VALUES
(1, 1, 146, 1, '2019-01-15'),
(2, 2, 315, 1, '2019-01-15'),
(3, 3, 409, 1, '2019-01-15'),
(4, 4, 1103, 1, '2019-01-15'),
(5, 5, 1105, 1, '2019-01-15'),
(6, 6, 808, 1, '2019-02-15'),
(7, 7, 1305, 50, '2019-02-15'),
(8, 8, 2501, 1, '2019-02-15'),
(9, 9, 315, 1, '2019-03-15'),
(10, 10, 320, 2, '2019-03-15'),
(11, 11, 321, 2, '2019-03-15'),
(12, 12, 322, 2, '2019-03-15');

-- --------------------------------------------------------

--
-- Table structure for table `stok_obat`
--

CREATE TABLE `stok_obat` (
  `id` int(11) NOT NULL,
  `batch_no` int(11) NOT NULL,
  `no_obat` int(11) NOT NULL,
  `jumlah_stok` int(11) NOT NULL,
  `stokpoligigi` int(11) DEFAULT NULL,
  `stokgudang` int(11) DEFAULT NULL,
  `exp_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_obat`
--

INSERT INTO `stok_obat` (`id`, `batch_no`, `no_obat`, `jumlah_stok`, `stokpoligigi`, `stokgudang`, `exp_date`) VALUES
(1, 1, 146, 2, 2, 0, '2019-12-12'),
(2, 2, 315, 1, 1, 0, '2019-12-12'),
(3, 3, 409, 1, 0, 1, '2019-12-12'),
(4, 4, 617, 1, 0, 1, '2019-12-12'),
(5, 5, 1103, 1, 0, 1, '2019-12-12'),
(6, 6, 1105, 1, 0, 1, '2019-12-12'),
(7, 7, 1113, 1, 0, 1, '2019-12-12'),
(8, 8, 1217, 15, 0, 15, '2019-12-12'),
(9, 9, 1305, 1, 0, 1, '2019-12-12'),
(10, 10, 1305, 28, 0, 28, '2019-12-12'),
(11, 11, 1831, 5, 0, 5, '2019-12-12'),
(12, 12, 2501, 2, 0, 2, '2019-12-12'),
(13, 13, 1, 1, 0, 1, '2019-12-12'),
(14, 14, 146, 1, 1, 0, '2019-12-12'),
(15, 15, 808, 1, 0, 1, '2019-12-12'),
(16, 16, 1103, 1, 0, 1, '2019-12-12'),
(17, 17, 1305, 30, 0, 30, '2019-12-12'),
(18, 18, 2501, 1, 0, 1, '2019-12-12'),
(19, 19, 320, 2, 2, 0, '2019-12-12'),
(20, 20, 321, 2, 2, 0, '2019-12-12'),
(21, 21, 322, 2, 1, 1, '2019-12-12'),
(22, 22, 315, 2, 2, 0, '2019-12-12'),
(23, 23, 509, 1, 0, 1, '2019-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(67) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(67) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idlevel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `idlevel`) VALUES
(1, 'admin', 'admin123', 0),
(5, 'kepala', 'admin2', 3),
(6, 'poligigi', 'admin3', 1),
(7, 'gudang', 'admin4', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_level`
--

CREATE TABLE `user_level` (
  `id` int(11) NOT NULL,
  `level` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_level`
--

INSERT INTO `user_level` (`id`, `level`) VALUES
(0, 'superadmin'),
(1, 'Poligigi'),
(2, 'Gudang'),
(3, 'Kepala');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id_obat`);

--
-- Indexes for table `obat_tipe`
--
ALTER TABLE `obat_tipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obat_trans`
--
ALTER TABLE `obat_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obat_transtipe`
--
ALTER TABLE `obat_transtipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemakaian`
--
ALTER TABLE `pemakaian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok_obat`
--
ALTER TABLE `stok_obat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `obat`
--
ALTER TABLE `obat`
  MODIFY `id_obat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `obat_tipe`
--
ALTER TABLE `obat_tipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `obat_trans`
--
ALTER TABLE `obat_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `obat_transtipe`
--
ALTER TABLE `obat_transtipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pemakaian`
--
ALTER TABLE `pemakaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `penerimaan`
--
ALTER TABLE `penerimaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `permintaan`
--
ALTER TABLE `permintaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `stok_obat`
--
ALTER TABLE `stok_obat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_level`
--
ALTER TABLE `user_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
